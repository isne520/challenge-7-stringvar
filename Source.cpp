#include <iostream>
#include "strvar.h"


void conversation(int max_name_size);
//Continues converstation with user

int main()

{

	using namespace std;
	using namespace strvarken;
	conversation(30);

	system("pause");
	return 0;
}

//Demo function
void conversation(int max_name_size)
{
	using namespace std;
	using namespace strvarken;

	int alphabet = 0; // variable that use to collect the position of the character
	char input; // variable that use to collect the new character you want to input
	int nextchar = 0; // variable that use to collect the amount of character you want to copy after the selected character

	StringVar phrase1(max_name_size), phrase2(max_name_size); //Declare StringVar to collect the inputs from the user

	cout << "What is your first phrase?\n"; // Prompt the user enter the first input
	phrase1.input_line(cin);

	cout << "What is your second phrase?\n"; // Prompt the user enter the second input
	phrase2.input_line(cin);

	cout << "First Phrase = " << phrase1 << endl; // Show the user's first input
	cout << "Second Phrase = " << phrase2 << endl; // Show the user's second input

	if (phrase1 == phrase2) { //operator == use to check whether the first input and the second input are equal to each other or not
		cout << "First phrase and second phrase are the same." << endl;
	}
	else {
		cout << "First phrase and second phrase are different." << endl;
	}

	StringVar hold = phrase1 + phrase2; //operator + use to combine the first input and the second input together
	cout << "First Phrase together with second phrase = " << hold << endl;

	cout << "Input a position of the alphabet that you would like to change: "; //Prompt the user to input the alphabet that they would like to change
	cin >> alphabet;

	cout << "\nYour character is : " << hold.one_char(alphabet) << endl; //show the character on the position selected by the user by using one_char function

	cout << "\nInput the character you would like to change: "; //Prompt the user to input the new character that they would like to replace the old one
	cin >> input;
	hold.set_char(alphabet, input); //function for changing the character in the StringVar

	cout << "\nYour new phrase is : " << hold << endl;

	cout << "\nEnter the position of the character you would like to start copying: "; //Prompt the user to input the position of the character they're going to start copying
	cin >> alphabet;
	cout << "\nEnter the number of character you want to copy after the first character: "; //Prompt the user to input the amount of character that they want to copy after the selected character
	cin >> nextchar;

	cout << "Your copy is : ";
	hold.copy_piece(alphabet, nextchar);  // function for copying the substring 
}