﻿#ifndef STRVAR_H
#define STRVAR_H
#include <iostream>
using namespace std;
namespace strvarken
{
	class StringVar
	{
	public:
		StringVar(int size);
		//Constructor when size of string is specified.
		StringVar();
		//Constructor with default size of 100.
		StringVar(const char a[]);
		//Precondition: The array a contains characters and ‘\0′.
		//Constructor using array as values.
		StringVar(const StringVar& string_object);
		//Copy Constructor.
		~StringVar();
		//Destructor, returns memory to heap.
		int length() const;
		//Returns the length of the current string.
		void input_line(istream& ins);
		//Precondition: if ins is a file input stream attached to a file.
		//The next text up to ‘\n’, or the capacity of the stringvar is copied.
		friend ostream& operator <<(ostream& outs, const StringVar& the_string);
		//Precondition: if outs is a file it has been attached to a file.
		//Overloads the << operator to allow output to screen
		friend StringVar operator+(StringVar &phrase1, StringVar &phrase2); //function for combining 2 inputs together
		friend istream &operator >> (istream &input, StringVar &newf);  //function for operator >>
		friend bool operator==(StringVar &phrase1, StringVar &phrase2); //function for comparing 2 inputs whether ther are equal to each other or not
		void copy_piece(int alphabet, int nextchar); //function for copying a substring
		char one_char(int alphabet); //function for showing the character on the position selected by the user
		void set_char(int alphabet, char input); //function for changing the character on the position selected by the user


	private:
		char *value; //pointer to dynamic array that holds the string value.
		int max_length; //declared maximum length of the string.
	};
}//strvarken
#endif //STRVAR_H