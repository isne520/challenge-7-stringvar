//strvar.cpp

#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include "strvar.h"

using namespace std;

namespace strvarken
{
	istream &operator >> (istream &input, StringVar &newf) //function for operator >>
	{
		input >> newf.value;
		return input;
	}

	//Uses cstddef and cstdlib
	StringVar::StringVar(int size) : max_length(size)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	//Uses cstddef and cstdlib
	StringVar::StringVar() : max_length(100)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	// Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const char a[]) : max_length(strlen(a))
	{
		value = new char[max_length + 1];

		for (int i = 0; i < strlen(a); i++)
		{
			value[i] = a[i];
		}
		value[strlen(a)] = '\0';
	}

	//Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const StringVar& string_object) : max_length(string_object.length())
	{
		value = new char[max_length + 1];
		for (int i = 0; i < strlen(string_object.value); i++)
		{
			value[i] = string_object.value[i];
		}
		value[strlen(string_object.value)] = '\0';
	}

	StringVar::~StringVar()
	{
		delete[] value;
	}

	//Uses cstring
	int StringVar::length() const
	{
		return strlen(value);
	}

	//Uses iostream
	void StringVar::input_line(istream& ins)
	{
		ins.getline(value, max_length + 1);
	}

	void StringVar::copy_piece(int alphabet, int nextchar) //funtion for copying a substring
	{
		for (int i = alphabet - 1; i < ((alphabet - 1) + nextchar) + 1; i++) {
			cout << value[i];
		}
		cout << endl;
	}

	char StringVar::one_char(int alphabet) //function for showing the character on the position selected by the user
	{
		return value[alphabet - 1];
	}

	void StringVar::set_char(int alphabet, char input) //function for changing the character on the position selected by the user
	{
		value[alphabet - 1] = input;
	}

	//Uses iostream
	ostream& operator << (ostream& outs, const StringVar& the_string)
	{
		outs << the_string.value;
		return outs;
	}

	StringVar operator+(StringVar &phrase1, StringVar &phrase2)  //function for combining to inputs together

	{
		StringVar hold;
		for (int i = 0; i < phrase1.max_length + 1; i++)
		{
			hold.value[i] = phrase1.value[i];
		}
		for (int i = phrase1.length(); i < phrase2.max_length + 1; i++)
		{
			hold.value[i] = phrase2.value[i - phrase1.length()];
		}
		return hold;
	}

	bool operator==(StringVar &phrase1, StringVar &phrase2) //function for comparing both inputs whether they are equal to each other or not
	{
		string x = phrase1.value;
		string y = phrase2.value;

		if (x == y) {
			return true;
		}
		else {
			return false;
		}
	}
}//strvarken